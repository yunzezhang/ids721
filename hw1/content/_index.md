---
title: "About"
---

### Nice to see you here!
I am Raplis Zhang, a graduate student from Duke University

-  I'm going to graduate in May 2024
-  Love [Arknights](https://ak.hypergryph.com/)
-  Java & C++ & Python
-  [My GitHub Page](https://raplis.github.io/) 

[![Top-Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=raplis&layout=compact&theme=default_repocard)](https://github.com/raplis)

